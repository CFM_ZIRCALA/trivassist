# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.shortcuts import render
from django.utils import timezone
from .models import Post
from django.views.generic.edit import FormView
from django.contrib.auth.forms import AuthenticationForm
from django.contrib.auth import authenticate
from django.contrib.auth import login as login_user
from django.core.urlresolvers import reverse_lazy
from django.contrib.auth import logout
from django.contrib.auth.forms import UserCreationForm
from django.shortcuts import render, redirect
from django.http import HttpResponseRedirect
# Create your views here.
def index(request):
    #la gracia es que hacemos el request y se lo pasamos al html segun entiendo
    posts = Post.objects.filter(published_date__lte=timezone.now()).order_by('published_date')
    #en este caso retorna los post filtados por la fecha
    return render(request, 'pagina/index.html' , {'posts':posts})


def usuarios_list(request):
    usuarios=user.objects.all()


def login(request):
    if request.method == 'POST':
        formulario=AuthenticationForm(request.POST)
        if formulario.is_valid:
            usuario = request.POST['username']
            clave = request.POST['password']
            acceso = authenticate(username=usuario, password=clave)
            if acceso is not None:
                if acceso.is_active:
                    login_user(request, acceso)
                    return redirect( home)
                else:
                    return redirect(index)
            else:
                return redirect(index)

    else:
        formulario = AuthenticationForm()
    return render(request, 'pagina/login.html', {'formulario': formulario})




def signup(request):
    if request.method == 'POST':
        form = UserCreationForm(request.POST)
        if form.is_valid():
            form.save()
            username = form.cleaned_data.get('username')
            raw_password = form.cleaned_data.get('password1')

            user = authenticate(username=username, password=raw_password)
            login_user(request, user)
            return redirect(index)
    else:
        form = UserCreationForm()
    return render(request, 'pagina/signup.html', {'form': form})

def logout_page(request):
    logout(request)
    #esta funcion sirve para salir
    return HttpResponseRedirect('/')

def home(request):
    return render(request, 'pagina/home.html')

def password_reset(request):
    return redirect('password_reset')
