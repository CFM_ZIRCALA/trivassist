from django.conf.urls import url,include
from django.contrib.auth import login
from django.contrib.auth.views import password_reset, password_reset_done, password_reset_confirm, password_reset_complete
#no es necesario esto -- from django.contrib import admin
from . import views
#from mysite.core import views as core_views
urlpatterns = [
    url(r'^$', views.index),# pagina de inicio predeterminado
    url(r'^signup/$', views.signup , name='signup'),
    url(r'^login/$', views.login , name='login'),
    url(r'^logout/$', views.logout_page , name='logout_page'),
    url(r'^home/$', views.home , name='home'),#esta sera la pagina para los usuarios que inician
    url(r'^password_reset' , password_reset , {'template_name':'correo/password_reset_form.html',
    'email_template_name':'correo/password_reset_email.html'}, name='password_reset'),
    url(r'^password_reset_done' , password_reset_done , {'template_name':'correo/password_reset_done.html' },
    name='password_reset_done'),
    url(r'^(?P<uidb64>[0-9A-Za-z_\-]+)/(?P<token>.+)/$' , password_reset_confirm , {'template_name':'correo/password_reset_confirm.html' },
    name='password_reset_confirm'),
    url(r'^password_reset_complete' , password_reset_complete , {'template_name':'correo/password_reset_complete.html' },
    name='password_reset_complete'),



    #url(r'^$', views.Login.as_view(), name="login"),
]
